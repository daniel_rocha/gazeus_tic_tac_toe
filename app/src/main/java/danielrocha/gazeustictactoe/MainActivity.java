package danielrocha.gazeustictactoe;

import android.animation.Animator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import danielrocha.gazeustictactoe.model.Vitoria;
import danielrocha.gazeustictactoe.model.Vitorias;

public class MainActivity extends AppCompatActivity {

    private static int TOTAL_JOGADAS = 9;
    private boolean fim = false;
    private ImageButton[] iB = new ImageButton[9];
    private int jogadas, jogador = 0;
    private String[] pl = {"Você", "Android"};
    private int[] img = {R.drawable.ic_x, R.drawable.ic_o};
    private TextView textMessage;
    private RelativeLayout playAgainLayout;
    private Button playAgaintBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playAgainLayout = (RelativeLayout) findViewById(R.id.playAgainLayout);
        textMessage = (TextView) findViewById(R.id.textMessage);
        playAgaintBt = (Button) findViewById(R.id.playAgainBt);

        iB[0] = (ImageButton) findViewById(R.id.b0);
        iB[1] = (ImageButton) findViewById(R.id.b1);
        iB[2] = (ImageButton) findViewById(R.id.b2);
        iB[3] = (ImageButton) findViewById(R.id.b3);
        iB[4] = (ImageButton) findViewById(R.id.b4);
        iB[5] = (ImageButton) findViewById(R.id.b5);
        iB[6] = (ImageButton) findViewById(R.id.b6);
        iB[7] = (ImageButton) findViewById(R.id.b7);
        iB[8] = (ImageButton) findViewById(R.id.b8);

        prepareGame();
    }

    private void prepareGame() {
        jogadas = 0;
        jogador = 0;

        Vitoria l1 = new Vitoria(iB[0], iB[1], iB[2]);
        Vitoria l2 = new Vitoria(iB[3], iB[4], iB[5]);
        Vitoria l3 = new Vitoria(iB[6], iB[7], iB[8]);
        Vitoria c1 = new Vitoria(iB[0], iB[3], iB[6]);
        Vitoria c2 = new Vitoria(iB[1], iB[4], iB[7]);
        Vitoria c3 = new Vitoria(iB[2], iB[5], iB[8]);
        Vitoria d1 = new Vitoria(iB[0], iB[4], iB[8]);
        Vitoria d2 = new Vitoria(iB[2], iB[4], iB[6]);

        iB[0].setTag(new Vitorias(l1, c1, d1));
        iB[1].setTag(new Vitorias(l1, c2));
        iB[2].setTag(new Vitorias(l1, c3, d2));

        iB[3].setTag(new Vitorias(l2, c1));
        iB[4].setTag(new Vitorias(l2, c2, d1, d2));
        iB[5].setTag(new Vitorias(l2, c3));

        iB[6].setTag(new Vitorias(l3, c1, d2));
        iB[7].setTag(new Vitorias(l3, c2));
        iB[8].setTag(new Vitorias(l3, c3, d1));

        for (int i = 0; i < TOTAL_JOGADAS; i++) {
            iB[i].setEnabled(true);
            iB[i].setImageResource(android.R.color.transparent);
            iB[i].setBackgroundResource(R.drawable.black_line);
        }

        playAgaintBt.setVisibility(View.INVISIBLE);
        textMessage.setText(getResources().getString(R.string.jogador, pl[jogador]));
        fim = false;
    }

    public void clickListener(View v) {
        ImageButton b = (ImageButton) v;
        b.setImageResource(img[jogador]);
        b.setEnabled(false);

        Vitoria g = ((Vitorias) b.getTag()).addMove(jogador);

        if(g != null) {
            fimJogo(g.getImageButtons(), jogador);
        } else {
            verificaVelha(++jogadas);
        }

        if(jogadas % 2 != 0 && !fim) robotMove();
    }

    public void playAgainClickListener(View v) {
        prepareGame();
    }

    private void fimJogo(List<ImageButton> buttons, int jogador) {
        for (final ImageButton button : buttons)
            button.animate().rotationY(button.getRotationY()+360).setDuration(1000).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    button.setBackgroundResource(R.drawable.black_line_green_back);
                }

                @Override
                public void onAnimationEnd(Animator animator) {

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

        finishGame(getResources().getString(R.string.vencedor, pl[jogador]));
    }

    private void verificaVelha(int jogadas) {
        jogador = (jogador == 0) ? 1 : 0;
        if(jogadas == TOTAL_JOGADAS) {
            finishGame(getResources().getString(R.string.velha));
        } else {
            textMessage.setText(getResources().getString(R.string.jogador, pl[jogador]));
        }
    }

    private void robotMove() {
        blockButtonsClick();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(getRandomNumber(500, 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        clickListener(getRandomButton());
                        desblockButtonsClick();
                    }
                });
            }
        }).start();
    }

    private View getRandomButton() {

        int i = getRandomNumber(0 , TOTAL_JOGADAS);
        ImageButton view = iB[i];
        boolean b = view.isEnabled();
        if(b) return view;
        else return getRandomButton();
    }

    private int getRandomNumber(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min) + min;
    }

    private void finishGame(String message) {
        disableButtons();
        fim = true;
        textMessage.setText(message);
        playAgaintBt.setVisibility(View.VISIBLE);
    }

    private void disableButtons() {
        for (int b = 0; b < TOTAL_JOGADAS; b++)
            iB[b].setEnabled(false);
    }

    private void blockButtonsClick() {
        for (int b = 0; b < TOTAL_JOGADAS; b++)
            iB[b].setClickable(false);
    }

    private void desblockButtonsClick() {
        for (int b = 0; b < TOTAL_JOGADAS; b++)
            iB[b].setClickable(true);
    }

}
