package danielrocha.gazeustictactoe.model;

import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by danielrocha on 20/06/17.
 */

public class Vitoria {
    private List<ImageButton> imageButtons = new ArrayList<>();
    private int[] jogada = new int[2];

    public Vitoria(ImageButton...imageButtons) {
        Collections.addAll(this.imageButtons, imageButtons);

        this.jogada[0] = 0;
        this.jogada[1] = 0;
    }

    public Vitoria addMove(int jogador) {
        return (++jogada[jogador] < 3) ? null : this;
    }

    public List<ImageButton> getImageButtons() {
        return this.imageButtons;
    }
}
