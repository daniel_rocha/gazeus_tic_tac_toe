package danielrocha.gazeustictactoe.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by danielrocha on 20/06/17.
 */

public class Vitorias {
    private ArrayList<Vitoria> vitorias = new ArrayList<>();

    public Vitorias(Vitoria... vitorias) {
        Collections.addAll(this.vitorias, vitorias);
    }

    public Vitoria addMove(int jogador) {
        Vitoria g;
        for (Vitoria vitoria : vitorias) {
            g = vitoria.addMove(jogador);
            if (g != null) return g;
        }
        return null;
    }
}
